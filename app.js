const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
require('dotenv').config();

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

const app = express();

app.use(cors()); // open allow domain
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//app.use('/', indexRouter); //http://localhost:3000
//app.use('/users', usersRouter);//http://localhost:3000/users
app.use('/api/v1', indexRouter); //http://localhost:3000/api/v1
app.use('/api/v1/users', usersRouter); //http://localhost:3000/api/v1/users

//app.use('/v1/users', usersRouter1);//http://localhost:3000/users
//app.use('/v2/users', usersRouter2);//http://localhost:3000/users

module.exports = app;
