const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy,
  ExtractJwt = require("passport-jwt").ExtractJwt;

  const opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken(); // ตรวจสอบ key
  opts.secretOrKey = process.env.JWT_KEY; //ตรวจสอบ key ที่ .env ว่าใช่ของเราหรือไม่
  // opts.issuer = 'accounts.examplesoft.com';
  // opts.audience = 'yoursite.net';

  passport.use(
    new JwtStrategy(opts, async function (jwt_payload, done) { // ถ้าตรวจสอบว่าใช่ จะเข้ามาทำงานที่นี่
      try {
        const user = { user_id: jwt_payload.user_id };
        if (user) {
          return done(null, user); // ส่งข้อมูล user ไปกับ req
        }
      } catch (error) {
        done(error);
      }
    })
  );



// check authen
module.exports.isLogin = passport.authenticate("jwt", { session: false }); // ถ้าเอา isLoing ไปขวาง Routes ไหน ถ้า token ไม่ตรงจะเข้าไม่ได้
